package com.example.fath.yamamlogin.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


/**
 * Created by Saif on 1/25/2017.
 */

public class PermissionsUtils {

    public static boolean requestPermission(String permission, Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission},
                        2);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
