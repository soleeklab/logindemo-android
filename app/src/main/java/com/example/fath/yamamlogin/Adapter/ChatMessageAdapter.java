package com.example.fath.yamamlogin.Adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.fath.yamamlogin.IntroActivity;
import com.example.fath.yamamlogin.MainActivity;
import com.example.fath.yamamlogin.R;
import com.example.fath.yamamlogin.callbacks.ChangePhoneNumber;
import com.example.fath.yamamlogin.callbacks.OnEditClicked;
import com.example.fath.yamamlogin.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatMessageAdapter extends RecyclerView.Adapter {
    private static final int MY_MESSAGE = -1, OTHER_MESSAGE = -2, LOADING = -3;
    boolean edit = true;
    OnEditClicked mOnclick;
    ChangePhoneNumber mOnChange;
    private Context mContext;
    public ArrayList<ChatMessage> data;
    CountDownTimer codeTimer;
    TextView timerSecTv, timerMinTv, changeBtn;
    Button resendBtn;
    int count = 0;

    public ChatMessageAdapter(Context context, List<ChatMessage> data, OnEditClicked mOnclick, ChangePhoneNumber mOnChange) {
        this.mOnclick = mOnclick;
        this.mOnChange = mOnChange;
        this.mContext = context;
        this.data = (ArrayList<ChatMessage>) data;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //return view holder according to the view type.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("viewtype", viewType + "");

        if (viewType == MY_MESSAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_mine_message, parent, false);
            return new mineView(view);
        } else if (viewType == OTHER_MESSAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_other_message, parent, false);
            return new otherView(view);
        } else if (viewType == LOADING) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_typing_indicator, parent, false);
            return new typingView(view);

        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_verfication_code, parent, false);
            return new verficationView(view);
        }
    }


    // bind view according to view type.
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ChatMessage item = data.get(position);

        if (holder instanceof mineView) {
            if (item.getContent() != null) {

                ((mineView) holder).mineTv.setText(item.getContent());

            }

        } else if (holder instanceof verficationView) {

            timerSecTv = holder.itemView.findViewById(R.id.txt_timer_sec);
            timerMinTv = holder.itemView.findViewById(R.id.txt_timer_min);
            ProgressBar timerProgress = ((verficationView) holder).progressBar;
            resendBtn = holder.itemView.findViewById(R.id.btn_resend);
            changeBtn = holder.itemView.findViewById(R.id.btn_change);
            // check if there an error message or not

            timerProgress.setMax(30);
            timerProgress.setProgress(0);
            startTimer(30 * 1000, timerProgress);

            //reset the count timer and increase the click count to change the progress bar according to it.
            resendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnChange.onChangeNumber(-1);
                }
            });

            changeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // call change number callback to repeat phone process.
                    mOnChange.onChangeNumber(position);
                    resendBtn.setVisibility(View.INVISIBLE);
                    changeBtn.setVisibility(View.INVISIBLE);
                }
            });
            SpannableString content = new SpannableString(mContext.getResources().getString(R.string.change_number));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            changeBtn.setText(content);


        } else if (holder instanceof otherView) {
            count++;
            ((otherView) holder).otherTv.setText(item.getContent());
            if (!data.get(position).isEditable()){
                ((otherView) holder).editImg.setVisibility(View.INVISIBLE);
            }
            if (changeBtn!=null){
            }
            //if (count>1&&position<=5)((otherView) holder).editImg.setVisibility(View.VISIBLE);
            //hide the first item till animation placed in.
            if (position == 1 && count == 1) {
                Log.d("position", position + "other");
                ((otherView) holder).otherTv.setVisibility(View.INVISIBLE);
                ((otherView) holder).editImg.setVisibility(View.INVISIBLE);
                ((otherView) holder).editImg.setEnabled(false);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        ((IntroActivity) mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((otherView) holder).otherTv.setVisibility(View.VISIBLE);
                                ((otherView) holder).editImg.setVisibility(View.VISIBLE);
                                ((otherView) holder).editImg.setEnabled(true);

                            }
                        });
                    }
                }, 600);
            }


            ((otherView) holder).editImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count = 0;
                    // call on edit click if the first edit clicked to change the sign process.
                    if (position == 1) {
                        mOnclick.onClick(position, null, true);
                    }
                    // call on edit click to allow the user edit his message.
                    else {
                        if (edit) {
                            mOnclick.onClick(position, ((otherView) holder).otherTv.getText().toString(), edit);
                            ((otherView) holder).otherTv.setFocusableInTouchMode(true);
                            ((otherView) holder).otherTv.selectAll();
                            ((otherView) holder).otherTv.requestFocus();
                            ((otherView) holder).cancelBtn.setVisibility(View.VISIBLE);
                            ((otherView) holder).cancelBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ((otherView) holder).otherTv.setFocusable(false);
                                    ((otherView) holder).otherTv.setText(data.get(position).getContent());
                                    mOnclick.onClick(position, ((otherView) holder).otherTv.getText().toString(), false);
                                    ((otherView) holder).editImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_edit));
                                    ((otherView) holder).cancelBtn.setVisibility(View.GONE);
                                    ((otherView) holder).errorTv.setVisibility(View.GONE);
                                    edit = true;
                                }
                            });
                            ((otherView) holder).editImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check_black_24dp));
                            edit = false;
                        } else {
                            if (((otherView) holder).otherTv.getText().toString() != null &&
                                    ((otherView) holder).otherTv.getText().toString().length() > 1 &&
                                    ((otherView) holder).otherTv.getText().toString().length() < 30) {
                                ((otherView) holder).editImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_edit));
                                ((otherView) holder).otherTv.setText(((otherView) holder).otherTv.getText().toString());
                                data.get(position).setContent(((otherView) holder).otherTv.getText().toString());
                                mOnclick.onClick(position, ((otherView) holder).otherTv.getText().toString(), edit);
                                ((otherView) holder).cancelBtn.setVisibility(View.GONE);
                                ((otherView) holder).otherTv.setFocusable(false);
                                ((otherView) holder).errorTv.setVisibility(View.GONE);
                                edit = true;
                            } else {
                                ((otherView) holder).errorTv.setVisibility(View.VISIBLE);
                                ((otherView) holder).errorTv.setText(mContext.getResources().getString(R.string.wrong_name));
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage item = data.get(position);
        if (item.isType()) return position;
        else if (item.getContent() == null) return LOADING;
        else if (item.getSender() == ChatMessage.Sender.human) return OTHER_MESSAGE;
        else if (item.getSender() == ChatMessage.Sender.bot) return MY_MESSAGE;
        return 0;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class mineView extends RecyclerView.ViewHolder {
        @BindView(R.id.text_mine_message)
        TextView mineTv;

        public mineView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class typingView extends RecyclerView.ViewHolder {
        public typingView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class otherView extends RecyclerView.ViewHolder {
        @BindView(R.id.text_other_message)
        EditText otherTv;
        @BindView(R.id.img_edit)
        ImageView editImg;
        @BindView(R.id.txt_error)
        TextView errorTv;
        @BindView(R.id.btn_cancel)
        Button cancelBtn;

        public otherView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class verficationView extends RecyclerView.ViewHolder {
        @BindView(R.id.progress_timer)
        ProgressBar progressBar;

        public verficationView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    //add new item to the list
    public void add(ChatMessage chatMessage) {
        data.add(chatMessage);
        notifyItemInserted(data.size());

    }

    //clear all data in the list
    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    //start count down timer for progress bar.
    public void startTimer(final long time, final ProgressBar progressBar) {
        codeTimer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(l);
                timerSecTv.setText(":" + seconds);
                int x = 30 - (int) seconds;

                progressBar.setProgress((int) x);


            }

            @Override
            public void onFinish() {

                resendBtn.setEnabled(true);
                changeBtn.setEnabled(true);
                resendBtn.setAlpha(1);
                changeBtn.setAlpha(1);

            }
        }.start();
    }

    public void removeLastItem() {

        if (data.get(data.size() - 1).getContent() != null && data.get(data.size() - 1).getContent().contains("Invalid verification code")) {
            return;
        }
        data.remove(data.size() - 1);
        notifyDataSetChanged();
        Log.d("remove", "removed");
    }
}
