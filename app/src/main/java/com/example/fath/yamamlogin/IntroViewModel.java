package com.example.fath.yamamlogin;

import android.util.Log;

import com.android.volley.Response;
import com.example.fath.yamamlogin.communications.VollyRequest;
import com.example.fath.yamamlogin.core.SessionManager;
import com.example.fath.yamamlogin.model.User;
import com.example.fath.yamamlogin.utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;


import static com.example.fath.yamamlogin.core.constants.BundleConstants.LOG_TAG;
import static com.example.fath.yamamlogin.core.constants.BundleConstants.debug;

public class IntroViewModel {

    IntroInterface introInterface;

    public IntroViewModel(IntroInterface introInterface) {
        this.introInterface = introInterface;
    }

    public boolean signup_request(String firstName, String lastName, String phoneNumber, String countryCode) {

        final boolean[] sucessRequest = {false};
        Response.Listener<JSONObject> sucess = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (debug) Log.d(LOG_TAG, response.toString());
                try {
                    String error = "";
                    if (response.getInt("code") != 200) {
                        error = response.get("message").toString();
                        introInterface.sendErrorMessage(error);
                    } else {
                        if (debug) Log.d(LOG_TAG + "code", response.getInt("code") + "");
                        User user = User.fromJson(response.toString());
                        SessionManager.setNewSession(user);
                        introInterface.successAuth();
                        sucessRequest[0] = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (debug) Log.d(LOG_TAG + "error", e.getMessage());
                }
            }
        };
        if (NetworkUtil.isConnected()) {
            VollyRequest.register_request(firstName, lastName, phoneNumber, countryCode, sucess);
        } else {
            Utils.showNoConnectionDialog(YamamApplication.get());
        }
        return sucessRequest[0];
    }
    //send request for login with phone number
    public boolean login_request(String phoneNumber, String countryCode) {

        final boolean[] sucessRequest = {false};
        Response.Listener<JSONObject> sucess = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (debug) Log.d(LOG_TAG, response.toString());
                try {
                    String error = "";
                    if (response.getInt("code") != 200) {
                        error = response.get("message").toString();
                        introInterface.sendErrorMessage(error);
                    } else {
                        if (debug) Log.d(LOG_TAG + "code", response.getInt("code") + "");
                        User user = User.fromJson(response.toString());
                        SessionManager.setNewSession(user);
                        introInterface.successAuth();
                        sucessRequest[0] = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (debug) Log.d(LOG_TAG + "error", e.getMessage());
                }
            }
        };
        if (NetworkUtil.isConnected()) {
            VollyRequest.login(phoneNumber, countryCode, sucess);
        } else {
            Utils.showNoConnectionDialog(YamamApplication.get());
        }
        return sucessRequest[0];
    }

    public boolean verify_request(String phoneNumber, String countryCode, String verficatonCode) {
        if (debug) Log.d(LOG_TAG+"verify", verficatonCode+""+phoneNumber+"//"+countryCode);

        final boolean[] sucessRequest = {false};
        Response.Listener<JSONObject> sucess = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (debug) Log.d(LOG_TAG, response.toString());
                //User user = User.fromJson(response.toString());
                try {
                    String error = "";
                    if (response.getInt("code") != 200) {
                        error = response.get("message").toString();
                        introInterface.sendErrorMessage(error);
                    } else {
                        if (debug) Log.d(LOG_TAG + "code", response.getInt("code") + "");
                        introInterface.sucessVerify();
                        SessionManager.setLoggedInStatus(true);
                        SessionManager.setUserToken(response.getJSONObject("data").getString("token"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (debug) Log.d(LOG_TAG + "error", e.getMessage());
                }
            }
        };
        if (NetworkUtil.isConnected()) {
            VollyRequest.verify(phoneNumber, countryCode, verficatonCode, sucess);
        } else {
            Utils.showNoConnectionDialog(YamamApplication.get());
        }
        return sucessRequest[0];
    }
}
