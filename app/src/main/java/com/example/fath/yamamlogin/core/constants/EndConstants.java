package com.example.fath.yamamlogin.core.constants;


public class EndConstants {

    public static final String API_BASE_URL = "http://52.174.22.188/yamam/public/api/";
    public static final String LOGIN_URL = API_BASE_URL+"auth";
    public static final String REGISTER_URL = API_BASE_URL+"auth/signup";
    public static final String VERIFY_URL = API_BASE_URL+"auth/verify";
    public static final String SAVED_PLACES_URL = API_BASE_URL+"user/places";


}


