package com.example.fath.yamamlogin;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.fath.yamamlogin.utils.CountryDetails;
import com.example.fath.yamamlogin.utils.NetworkUtil;

import static com.example.fath.yamamlogin.utils.CountryDetails.country;

/**
 * Created by fath_soleeklab on 2/6/2018.
 */

public class Utils {

    public static void hideSoftKeyboard(Activity activity, EditText editText) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
               editText.getWindowToken(), 0);
    }

    public static void showNoConnectionDialog(final Context ctx1) {

        if(NetworkUtil.isConnected()) {

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx1);
            builder.setCancelable(false);
            builder.setTitle("No Internet");
            builder.setMessage("Internet is required. Please Retry.");

            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    showNoConnectionDialog(ctx1);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static String getCountryDialCode(Context context){
        String contryId = null;
        String contryDialCode = null;
        String countryName = context.getResources().getConfiguration().locale.getDisplayCountry();

        for (int i = 0;i< country.length;i++){
            if (country[i].contains("Egypt") ){
                contryDialCode = CountryDetails.code[i];
            }
        }
        return contryDialCode;
    }
}
