package com.example.fath.yamamlogin.callbacks;

/**
 * Created by fath_soleeklab on 2/12/2018.
 */

public interface OnCountrySelect {
    void onSelect (String code, int position);
}
