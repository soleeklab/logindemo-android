package com.example.fath.yamamlogin.core;


import com.example.fath.yamamlogin.core.constants.PrefsConstants;
import com.example.fath.yamamlogin.model.User;
import com.example.fath.yamamlogin.utils.PrefsUtil;

public class SessionManager {
    /**
     * Returns a valid apiKey for session
     */
    public static String getUserToken(){
        return PrefsUtil.getString(PrefsConstants.USER_TOKEN);
    }

    public static void setUserToken(String userToken){
        PrefsUtil.saveString(PrefsConstants.USER_TOKEN,userToken);
    }
    public static String getUserId(){
        return PrefsUtil.getString(PrefsConstants.USER_ID);
    }

    private static void setUserId(String userToken){
        PrefsUtil.saveString(PrefsConstants.USER_ID,userToken);
    }
    public static String getUserName(){
        return PrefsUtil.getString(PrefsConstants.USER_NAME);
    }

    private static void setUserName(String userName){
        PrefsUtil.saveString(PrefsConstants.USER_NAME,userName);
    }
    public static String getUserEmail(){
        return PrefsUtil.getString(PrefsConstants.USER_EMAIL);
    }

    private static void setUserEmail(String userEmail){
        PrefsUtil.saveString(PrefsConstants.USER_EMAIL,userEmail);
    }
    public static String getUserImage(){
        return PrefsUtil.getString(PrefsConstants.USER_IMAGE);
    }

    private static void setUserImage(String userImage){
        PrefsUtil.saveString(PrefsConstants.USER_IMAGE,userImage);
    }

    public static void setNewSession(User userModel) {
        setUserId(String.valueOf(userModel.getUser().getId()));
        setUserName(userModel.getUser().getFirstName()+" "+userModel.getUser().getLastName());
    }

    public static void setLoggedInStatus(boolean isLoggedIn) {
        PrefsUtil.saveBoolean(PrefsConstants.IS_LOGGED_KEY,isLoggedIn);
    }
public static boolean isLoggedIn (){
        return PrefsUtil.getBoolean(PrefsConstants.IS_LOGGED_KEY);
}
    public static void clearCurrentSession() {
        setUserToken(null);
        setUserName(null);
        setUserEmail(null);
        setUserImage(null);
        setLoggedInStatus(false);
    }


                    }





