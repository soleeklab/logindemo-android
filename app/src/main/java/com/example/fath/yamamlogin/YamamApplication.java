package com.example.fath.yamamlogin;

import android.app.Application;

import com.example.fath.yamamlogin.utils.ConnectivityReceiver;


public class YamamApplication extends Application {

    private static YamamApplication instance;
    private Thread.UncaughtExceptionHandler defaultUEH;

    public static YamamApplication get() {
        if (instance == null)
            throw new IllegalStateException("Application instance has not been initialized!");
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

}
