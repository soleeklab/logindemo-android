package com.example.fath.yamamlogin.model;



public class ChatMessage {
    public enum Sender{
        bot,human
    }
    private boolean type,isEditable;
    private String content;
    private Sender sender;
    public ChatMessage(String message,Sender sender,boolean isEditable)
    {
        content = message ;
        this.sender = sender;
        type = false ;
        this.isEditable = isEditable;
    }

    private ChatMessage( Sender sender , boolean type) {
        this.sender = sender;
        this.type = type;
    }


    public static ChatMessage progressBar = new ChatMessage(Sender.bot,true);

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }
}
