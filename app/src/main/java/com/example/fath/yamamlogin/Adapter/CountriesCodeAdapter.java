package com.example.fath.yamamlogin.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.example.fath.yamamlogin.R;
import com.example.fath.yamamlogin.callbacks.OnCountrySelect;
import com.example.fath.yamamlogin.model.Country;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by fath_soleeklab on 2/12/2018.
 */

public class CountriesCodeAdapter extends RecyclerView.Adapter<CountriesCodeAdapter.viewHolder> implements Filterable {
    ArrayList<Country> countries;
    Context mContext;
    OnCountrySelect onCountrySelect;
    ArrayList<Country> countriesFiltered;

    public CountriesCodeAdapter(ArrayList<Country> countries, Context mContext, OnCountrySelect onCountrySelect) {
        this.countries = countries;
        this.mContext = mContext;
        this.onCountrySelect = onCountrySelect;
        this.countriesFiltered = countries;
    }

    @Override
    public CountriesCodeAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_country_code, parent, false);
        return new CountriesCodeAdapter.viewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountriesCodeAdapter.viewHolder holder, int position) {
        Country country = countriesFiltered.get(position);
        holder.countryCodeTv.setText("+" + country.getCountryCode());
        holder.countryNameTv.setText(country.getCountryName());

    }

    @Override
    public int getItemCount() {
        return countriesFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.d("filter", charString);
                if (charString.isEmpty()) {
                    countriesFiltered = countries;
                } else {
                    List<Country> filteredList = new ArrayList<>();
                    for (Country row : countries) {


                        if (row.getCountryName().toLowerCase().contains(charString.toLowerCase()) || row.getCountryCode().contains(charSequence)) {
                            filteredList.add(row);
                            Log.d("filter1", charString);

                        }
                    }

                    countriesFiltered = (ArrayList<Country>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = countriesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                countriesFiltered = (ArrayList<Country>) filterResults.values;
                Log.d("filter12", charSequence.toString());

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_country_name)
        TextView countryNameTv;
        @BindView(R.id.txt_country_code)
        TextView countryCodeTv;

        public viewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCountrySelect.onSelect(countriesFiltered.get(getAdapterPosition()).getCountryCode(), getAdapterPosition());
                }
            });
        }
    }
}

