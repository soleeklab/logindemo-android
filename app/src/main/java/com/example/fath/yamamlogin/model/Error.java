package com.example.fath.yamamlogin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fath_soleeklab on 2/18/2018.
 */

public class Error implements Serializable {

    @SerializedName("errorMessage")
    private String errorMessage;
    @SerializedName("errorDetails")
    private String errorDetails;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }
}
