package com.example.fath.yamamlogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_TIME = 3000;
    private final static int SPLASH_THREAD_FINISHED = 1;
    @BindView(R.id.splash_logo)ImageView logoIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        startAnimation();
        startSplashTimer();
    }

    private void startSplashTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                splashDirection();
            }
        }, SPLASH_TIME);
    }


    private void splashDirection() {

            startActivity(new Intent(this,IntroActivity.class));
            finish();
    }

    private void startAnimation() {
        final Animation aniIn = AnimationUtils.loadAnimation(this,
                R.anim.animation_fade_in);

        aniIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                logoIv.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //logoIv.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logoIv.startAnimation(aniIn);
                    }
                });
            }
        }, 600);
}}
