package com.example.fath.yamamlogin.communications;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.fath.yamamlogin.R;
import com.example.fath.yamamlogin.YamamApplication;
import com.example.fath.yamamlogin.core.SessionManager;
import com.example.fath.yamamlogin.core.constants.EndConstants;
import com.example.fath.yamamlogin.utils.NetworkUtil;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.fath.yamamlogin.core.constants.BundleConstants.LOG_TAG;
import static com.example.fath.yamamlogin.core.constants.BundleConstants.debug;

public class VollyRequest {

    public static void vollyRequst(String URL, JSONObject parameters, int method, Response.Listener<JSONObject> sucess){
        if (NetworkUtil.isConnected()){
        final RequestQueue queue = Volley.newRequestQueue(YamamApplication.get());
        final JsonObjectRequest request = new JsonObjectRequest(method, URL, parameters, sucess, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOG_TAG, "message: " + error.getMessage());
                try {
                if(error.networkResponse.data!=null) {

                        JSONObject response = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        //Toast.makeText(YamamApplication.get(), response.getString("userMessage"), Toast.LENGTH_LONG).show();
                        //if (debug) Log.d(LOG_TAG, "message: " + new String(error.networkResponse.data, "UTF-8"));
                }
                    } catch (Exception e) {
                         //Log.e(LOG_TAG, "message: " + e.getMessage());
                    }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + SessionManager.getUserToken());
                if (debug) Log.d(LOG_TAG+" /token =", SessionManager.getUserToken()+"");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }
    else {
            //Utils.showNoConnectionDialog(YamamApplication.get());
            Toast.makeText(YamamApplication.get(),YamamApplication.get().getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();
        }
    }

    public static void register_request (String firstName , String lastName, String phoneNumber , String countryCode, Response.Listener sucess){

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("first_name", firstName);
            parameters.put("last_name", lastName);
            parameters.put("phone_number", phoneNumber);
            parameters.put("country_code", countryCode);
        } catch (Exception e) {
            Log.d("",e.getMessage());
        }
        String URL = EndConstants.REGISTER_URL;
        int method = Request.Method.POST;

        vollyRequst( URL,  parameters,  method,sucess);
    }

    public static void login (String phoneNumber, String countryCode, Response.Listener sucess){

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("phone_number", phoneNumber);
            parameters.put("country_code", countryCode);

        } catch (Exception e) {
            Log.d("",e.getMessage());
        }
        String URL = EndConstants.LOGIN_URL;
        int method = Request.Method.POST;

        vollyRequst( URL,  parameters,  method,sucess);
    }
    public static void verify (String phoneNumber, String countryCode, String verificationCode, Response.Listener sucess){
        Log.d("params",phoneNumber+countryCode+verificationCode);

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("phone_number", phoneNumber);
            parameters.put("country_code", countryCode);
            parameters.put("verification_code", verificationCode);


        } catch (Exception e) {
            Log.d("",e.getMessage());
        }
        String URL = EndConstants.VERIFY_URL;
        int method = Request.Method.POST;

        vollyRequst( URL,  parameters,  method,sucess);
    }

}
